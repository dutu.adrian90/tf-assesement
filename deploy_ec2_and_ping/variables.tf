variable "configuration" {
  description = "Configuration for instances"
  type = map(object({
    ami            = string
    instance_type  = string
  }))
  validation {
    condition = length(var.configuration) >= 2 && length(var.configuration) <= 100  
    error_message = "The map must contain between 2 and 100 elements"
  }
}

