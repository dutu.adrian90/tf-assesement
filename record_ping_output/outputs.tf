output "ping_results" {
  value = [
    for key in keys(data.external.read_file_and_empty.result) : {
      key   = key
      value = data.external.read_file_and_empty.result[key]
    }
  ]
}
